package matok

import (
	"bytes"
	"crypto/sha256"
	"encoding/ascii85"
	"hash"
	"reflect"
	"sync"
	"unsafe"
)

var hashPool = sync.Pool{
	New: func() interface{} {
		return sha256.New()
	},
}

type Manager struct {
	secret []byte
}

func NewManager(secret string) *Manager {
	return &Manager{secret: []byte(secret)}
}

// Encode encodes ascii85(dat) + '~' + ascii85(sha256(dat + secret)) in the dst slice.
//
// Requires single Init or InitBytes invocation to work.
func (m *Manager) Encode(dst, dat []byte) []byte {
	nMax := ascii85.MaxEncodedLen(len(dat))
	if cap(dst) < nMax+321 /* len("~") + 256 bit signature */ {
		dst = make([]byte, nMax, nMax+321)
	}
	// ascii85.Encode does not append
	dst = unsafeSetLen(dst, nMax)

	nDat := ascii85.Encode(dst, dat)
	dst = unsafeSetLen(dst, nDat)

	// ascii85 doesn't use '~'
	dst = append(dst, '~')

	dst = unsafeSetLen(dst, nDat+1+320)

	h := hashPool.Get().(hash.Hash)
	h.Write(dat)
	h.Write(m.secret)

	sig := h.Sum(make([]byte, 0, 32))
	h.Reset()
	hashPool.Put(h)

	nSig := ascii85.Encode(dst[nDat+1:], sig)
	return unsafeSetLen(dst, nDat+1+nSig)
}

// DecodeReuse verifies the given token against the singing secret
// and decodes its contents to the tok.Data array.
//
// Requires single Init or InitBytes invocation to work.
func (m *Manager) DecodeReuse(tok []byte) ([]byte, bool) {
	sep := bytes.IndexByte(tok, '~')
	if sep == -1 || sep == len(tok)-1 {
		return nil, false
	}

	a85dat := tok[:sep]
	a85sig := tok[sep+1:]

	if len(a85sig) > 40 /* ascii85.MaxEncodedLen(256) */ {
		return nil, false
	}

	var (
		dat, sig []byte
		ok       bool
	)
	if dat, ok = decodeA85(tok[:sep], a85dat); !ok {
		return nil, false
	}
	if sig, ok = decodeA85(tok[sep+1:], a85sig); !ok {
		return nil, false
	}

	h := hashPool.Get().(hash.Hash)
	h.Write(dat)
	h.Write(m.secret)

	refSig := h.Sum(make([]byte, 0, 32))
	h.Reset()
	hashPool.Put(h)

	ok = bytes.Equal(sig, refSig)
	return dat, ok
}

func decodeA85(dst, dat []byte) ([]byte, bool) {
	n, _, err := ascii85.Decode(dst, dat, true)
	return dst[:n], err == nil
}

func unsafeSetLen(b []byte, newLen int) []byte {
	sh := *(*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh.Len = newLen
	return *(*[]byte)(unsafe.Pointer(&sh))
}
