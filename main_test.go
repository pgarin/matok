package matok

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/bytebufferpool"
)

type td struct {
	dat []byte
	tok []byte
}

func tdOk() td {
	return td{
		dat: []byte("{\"name\":\"Pyotr Garin\",\"mode\":255}"),
		tok: []byte("HQm?9D.OnP,#EQ'FE/Ko@<-(\"+tOpTDe*E'3\\iQMI/~m\\P+`bc0I:2PkQqj/5;A:]8RRkc#e3V;d;KC$EV7"),
	}
}

var m = NewManager("O7DQ7yHnqaIJ4oNdS98ZDvCTah2tfb6CBO8_vID-VxY")

func TestDecodeReuse(t *testing.T) {
	type tc struct {
		name  string
		td    td
		valid bool
	}
	tcs := []tc{
		{"ok", tdOk(), true},
		{"sep", td{
			dat: nil,
			tok: []byte("~"),
		}, false},
		{"nil", td{dat: nil, tok: nil}, false},
	}

	pool := bytebufferpool.Pool{}
	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			buf := pool.Get()
			defer pool.Put(buf)

			tokCpy := make([]byte, len(tc.td.tok))
			copy(tokCpy, tc.td.tok)

			dat, valid := m.DecodeReuse(tokCpy)
			assert.Equal(t, tc.valid, valid)
			if !valid {
				return
			}

			assert.Equal(t, tc.td.dat, dat)
			assert.NotEqual(t, tc.td.tok, tokCpy)
		})
	}
}

func BenchmarkEncode(b *testing.B) {
	td := tdOk()
	pool := bytebufferpool.Pool{}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf := pool.Get()
		_ = m.Encode(buf.B, td.dat)
		pool.Put(buf)
	}
	b.ReportAllocs()
}

func BenchmarkDecodeReuse(b *testing.B) {
	tokList := make([][]byte, 0, b.N)
	for i := 0; i < b.N; i++ {
		tokList = append(tokList, tdOk().tok)
	}
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = m.DecodeReuse(tokList[i])
	}
	b.ReportAllocs()
}
