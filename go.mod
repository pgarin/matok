module gitlab.com/pgarin/matok

go 1.16

require (
	github.com/stretchr/testify v1.7.0
	github.com/valyala/bytebufferpool v1.0.0
)
