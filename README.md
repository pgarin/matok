# Matok = MAC Token
Somewhat like a JWT with hardcoded defaults. Not suitable for query passing. Fits nicely in `Authorization` header and cookies.

# Usage
```go
m := Matok.NewManager(secret)
```

## Encode
```go
// getToken.go
tokPool := bytebufferpool.Pool{}

...
buf := tokPool.Get()
defer tokPool.Put(buf)

tok := m.Encode(buf.B, userClaims)
ctx.Write(tok)
```

## Decode
```go
// middleware/auth.go
tok := ctx.Request.Header.Peek("Authorization")
if len(tok) < 7 {
	...
}
tok = tok[7:] // The "Bearer " thing
dat, ok := m.DecodeReuse(tok)
...
```

See https://gitlab.com/pgarin/matok/-/blob/master/main_test.go

## Bench
```
goos: linux
goarch: amd64
pkg: gitlab.com/pgarin/matok
cpu: AMD Ryzen 5 3500X 6-Core Processor             
BenchmarkEncode
BenchmarkEncode-6        	 1000000	      1105 ns/op	     416 B/op	       2 allocs/op
BenchmarkDecodeReuse
BenchmarkDecodeReuse-6   	 1996802	       594.0 ns/op	      32 B/op	       1 allocs/op
```

JWT libraries I've seen do just about the same
```
BenchmarkHS256Signing-6   	 1599536	       721.1 ns/op	    1544 B/op	      32 allocs/op
```

Most of them don't include decode bench^
```go
func BenchmarkJWTDecode(b *testing.B) {
	tokenString := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiUHlvdHIgR2FyaW4iLCJtb2RlIjoyNTV9.eP3J7iMNyWOv8DeU3jtL1aXNUWtV9ArXa_heivaNpEs"
	for i := 0; i < b.N; i++ {
		_, _ = jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			return secret, nil
		})
	}
	b.ReportAllocs()
}
```
```
BenchmarkJWTDecode-6   	  127827	     10668 ns/op	    3376 B/op	      56 allocs/op
```
